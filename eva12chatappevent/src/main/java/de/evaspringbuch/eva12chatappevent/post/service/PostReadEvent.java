package de.evaspringbuch.eva12chatappevent.post.service;

public record PostReadEvent(String from, String to) {
}