package de.evaspringbuch.eva12chatappevent.post.service.dto;

public record PostDTO(String content, String timestamp, String type, String read) {

	@Override
	public String toString() {
		return "PostDTO{" + "content=" + content + " " + "timestamp=" + timestamp + " " + "type=" + type + " " + "read="
				+ read + '}';
	}

}