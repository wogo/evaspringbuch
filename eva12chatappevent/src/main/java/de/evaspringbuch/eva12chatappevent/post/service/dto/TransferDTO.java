package de.evaspringbuch.eva12chatappevent.post.service.dto;

import java.io.Serializable;

public record TransferDTO(

		String to, int amount) implements Serializable {
}
