package de.evaspringbuch.eva12chatappevent.post.service.dto;

public record AccountResponseDTO(String code) {
}
