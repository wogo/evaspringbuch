package de.evaspringbuch.eva12chatappevent.security.service.dto;

public record UserDTO(

		Long id, String nickname, String email) {
}
