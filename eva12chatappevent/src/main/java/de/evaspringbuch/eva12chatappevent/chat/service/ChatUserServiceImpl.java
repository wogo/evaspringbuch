package de.evaspringbuch.eva12chatappevent.chat.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.evaspringbuch.eva12chatappevent.chat.domain.Chat;
import de.evaspringbuch.eva12chatappevent.chat.domain.ChatUser;
import de.evaspringbuch.eva12chatappevent.chat.domain.ChatUserRepository;
import de.evaspringbuch.eva12chatappevent.chat.service.dto.ChatDTO;
import de.evaspringbuch.eva12chatappevent.security.domain.UserCreateForm;

@Service
public class ChatUserServiceImpl implements ChatUserService {

	private ChatUserRepository chatUserRepository;

	private ChatService chatService;

	@Autowired
	public ChatUserServiceImpl(ChatUserRepository chatUserRepository, ChatService chatService) {
		this.chatUserRepository = chatUserRepository;
		this.chatService = chatService;
	}

	@Override
	public void createChatUser(UserCreateForm form) {
		ChatUser chatUser = new ChatUser();
		chatUser.setNickname(form.getNickname());
		chatUserRepository.save(chatUser);
	}

	@Override
	public ChatUser getByNickname(String str) {
		return chatUserRepository.findByNickname(str).orElse(null);
	}

	@Override
	public boolean existsNickname(String to) {
		return chatUserRepository.existsByNickname(to);
	}

	@Override
	public boolean chatUserChatsContainsKeyfindByNickname(String from, String to) {
		Optional<ChatUser> userToOpt = chatUserRepository.findByNickname(from);
		return userToOpt.map(chatUser -> chatUser.getChats().containsKey(to)).orElse(false);
	}

	@Override
	public void newChatFromTo(String from, String to) {
		ChatUser userFrom = chatUserRepository.findByNickname(from).orElse(null);
		ChatUser userTo = chatUserRepository.findByNickname(to).orElse(null);
		chatService.newChatBetween(userTo, userFrom);
	}

	@Override
	public void deleteChatFromTo(String from, String to) {
		ChatUser userFrom = chatUserRepository.findByNickname(from).orElse(null);
		ChatUser userTo = chatUserRepository.findByNickname(to).orElse(null);
		chatService.deleteChatBetween(userFrom, userTo);
	}

	@Override
	public Chat getChatFromByNicknameTo(String from, String to) {
		Optional<ChatUser> userToOpt = chatUserRepository.findByNickname(to);
		return userToOpt.map(chatUser -> chatUser.getChats().get(from)).orElse(null);

	}

	@Override
	public List<ChatDTO> getAllChatFrom(String from) {
		ChatUser userFrom = getByNickname(from);
		return userFrom.getChats().values().stream()
				.map(source -> new ChatDTO(source.getChatWith(), source.getNewPosts()))
				.sorted(Comparator.comparing(ChatDTO::chatWith)).collect(Collectors.toList());

	}

	@Override
	public NewChatPossible aNewChatPossible(String from, String to) {
		if (to.equals("")) {
			return new NewChatPossible(false, "");
		} else if ((!this.existsNickname(to))) { // (to.equals("")) ||
			return new NewChatPossible(false, "der teilnehmer ist unbekannt !!!");
		} else if (this.chatUserChatsContainsKeyfindByNickname(from, to) || (to == " ")) {
			return new NewChatPossible(false, "du hast schon einen chat mit diesem teilnehmer");
		} else if (from.equals(to)) {
			return new NewChatPossible(false, "du darfst nicht mit dir selber chatten");
		}
		return new NewChatPossible(true, "");
	}

}
