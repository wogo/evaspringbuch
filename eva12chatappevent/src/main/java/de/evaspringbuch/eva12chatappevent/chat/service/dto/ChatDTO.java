package de.evaspringbuch.eva12chatappevent.chat.service.dto;

public record ChatDTO(String chatWith, int newPosts) {
}
