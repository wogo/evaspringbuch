package de.evaspringbuch.eva12chatappevent.chat.domain;

public enum ChatType {
	NORMAL, BOT
}
