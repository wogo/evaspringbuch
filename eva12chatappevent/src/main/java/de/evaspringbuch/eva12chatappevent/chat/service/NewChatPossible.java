package de.evaspringbuch.eva12chatappevent.chat.service;

public record NewChatPossible(Boolean possible, String message) {

}
