package de.evaspringbuch.eva10transaction.tryTransaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findByName(String name);
}


