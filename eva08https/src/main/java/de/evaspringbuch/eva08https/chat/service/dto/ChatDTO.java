package de.evaspringbuch.eva08https.chat.service.dto;

public record ChatDTO (String chatWith, int newPosts) {		
}

