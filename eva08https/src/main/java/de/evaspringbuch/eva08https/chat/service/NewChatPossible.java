package de.evaspringbuch.eva08https.chat.service;

public record NewChatPossible(Boolean possible, String message) {

}
