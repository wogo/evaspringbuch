package de.evaspringbuch.eva08https.chat.domain;

public enum ChatType {
    NORMAL, BOT
}
