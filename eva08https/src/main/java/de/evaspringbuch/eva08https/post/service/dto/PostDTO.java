package de.evaspringbuch.eva08https.post.service.dto;

public record PostDTO(String content, String timestamp, String type) {
}