package de.evaspringbuch.eva08https.security.service.dto;

public record UserDTO (
		Long id,
		String nickname,
		String email
		) {}
