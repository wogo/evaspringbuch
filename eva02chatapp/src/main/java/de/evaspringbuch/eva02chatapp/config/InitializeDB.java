package de.evaspringbuch.eva02chatapp.config;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.evaspringbuch.eva02chatapp.chat.domain.Chat;
import de.evaspringbuch.eva02chatapp.chat.domain.ChatRepository;
import de.evaspringbuch.eva02chatapp.chat.domain.ChatUser;
import de.evaspringbuch.eva02chatapp.chat.domain.ChatUserRepository;
import de.evaspringbuch.eva02chatapp.post.domain.Post;
import de.evaspringbuch.eva02chatapp.post.domain.PostRepository;
import jakarta.annotation.PostConstruct;

@Component
public class InitializeDB {

	private static final Logger log = LoggerFactory.getLogger(InitializeDB.class);

	@Autowired
	ChatUserRepository chatUserRepository;
	@Autowired
	ChatRepository chatRepository;
	@Autowired
	PostRepository postRepository;

	@PostConstruct
	public void init() {

		log.debug(" >>> Db initialized");

		ChatUser chatUser1 = createChatUser("elisa");
		ChatUser chatUser2 = createChatUser("marga");
		ChatUser chatUser3 = createChatUser("frieda");

		Chat chat1 = createChat(chatUser2.getNickname(), chatUser1);
		Chat chat2 = createChat(chatUser1.getNickname(), chatUser2);

		chatUser1.addChat(chat1);
		chatUser2.addChat(chat2);
		chatRepository.save(chat1);
		chatRepository.save(chat2);

		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)
				.withLocale(Locale.GERMAN);
		String s = LocalTime.now().minusMinutes(10).format(germanFormatter);

		createPost("Hallo Marga", s, chat1, "out");
		createPost("Hallo Marga", s, chat2, "in");

		s = LocalTime.now().minusMinutes(8).format(germanFormatter);

		createPost("Hi Elisa", s, chat1, "in");
		createPost("Hi Elisa", s, chat2, "out");

		chatRepository.save(chat1);
		chatRepository.save(chat2);
	}

	private ChatUser createChatUser(String nickname) {
		ChatUser chatUser = new ChatUser();
		chatUser.setNickname(nickname);
		chatUserRepository.save(chatUser);
		return chatUser;
	}

	private Chat createChat(String nickname, ChatUser chatUser) {
		Chat chat = new Chat(nickname, chatUser);
		chatUser.addChat(chat);
		chatRepository.save(chat);
//		try {
//			chatRepository.save(chat);
//			log.info("Chat between {} and {} created successfully", nickname, chatUser.getNickname());
//		} catch (Exception e) {
//			log.error("Error creating Chat between {} and {}: {}", nickname, chatUser.getNickname(), e.getMessage());
//		}
		return chat;
	}
	
	private void createPost(String content, String timestamp, Chat chat, String inOrOut) {
		Post post = new Post(content, timestamp, chat, inOrOut);
		postRepository.save(post);
		chat.addPosts(post);
//            if ("out".equals(direction)) {
//                chat.addNewPosts();
//            }
	}
}