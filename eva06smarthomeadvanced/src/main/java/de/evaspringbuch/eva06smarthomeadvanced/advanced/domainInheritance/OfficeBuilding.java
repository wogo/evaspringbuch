package de.evaspringbuch.eva06smarthomeadvanced.advanced.domainInheritance;

import de.evaspringbuch.eva06smarthomeadvanced.advanced.domain.Building;
import jakarta.persistence.Entity;

@Entity
public class OfficeBuilding extends Building {

	private String sector;

	public OfficeBuilding() {
	}

	public OfficeBuilding withSector(String sector) {
		this.sector = sector;
		return this;
	}
}
