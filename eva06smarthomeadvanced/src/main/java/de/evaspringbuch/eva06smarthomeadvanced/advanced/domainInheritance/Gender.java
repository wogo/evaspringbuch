package de.evaspringbuch.eva06smarthomeadvanced.advanced.domainInheritance;

public enum Gender {
	FEMALE, MALE;
}
