package de.evaspringbuch.eva06smarthomeadvanced.advanced.domain;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;

@Entity
public class Address implements Serializable {

	@Id
	@GeneratedValue
	private Integer id;

	private String city;

	@Embedded
	private AdvancedAddress advancedAddress = new AdvancedAddress();

	@OneToOne(mappedBy = "address")
	Building building;

	public Address() {
	}

	public Address(String city) {
		this.setCity(city);
	}

	public Integer getId() {
		return id;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public void setAdvancedAddress(String zipCode, String street) {
		this.advancedAddress.setZipcode(zipCode);
		this.advancedAddress.setStreet(street);
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		return this.getId() != null && this.getId().equals(other.getId());
	}
}
