package de.evaspringbuch.eva06smarthomeadvanced.advanced.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
// InheritanceType.SINGLE_TABLE, InheritanceType.TABLE_PER_CLASS; InheritanceType.JOINED
//@EntityListeners(BuildingListener.class)
public class Building implements Serializable {

	@Id
	@GeneratedValue // (strategy = GenerationType.TABLE)
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Room> rooms = new ArrayList<Room>();

	@OneToOne
	private Address address;

	@ManyToMany(mappedBy = "ownedBuildings")
	private List<Person> owners = new ArrayList<>();

	public Building() {
//        this.rooms = new ArrayList<>();
//        this.owners = new ArrayList<>();
	}

	public Integer getId() {
		return id;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public Address getAddress() {
		return address;
	}

	public Building addRoom(Room room) {
		this.rooms.add(room);
		return this;
	}

	public Building withAddress(Address address) {
		this.address = address;
		return this;
	}

	public List<Person> getOwners() {
		return owners;
	}

	public Building addOwner(Person owner) {
		this.owners.add(owner);
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Building other = (Building) obj;
		return this.getId() != null && this.getId().equals(other.getId());
	}

	@Override
	public String toString() {
		return "Building [id=" + id + ", rooms=" + rooms + ", address=" + address + ", owners=" + owners + "]";
	}

}
