package de.evaspringbuch.eva06smarthomeadvanced.advanced.domainInheritance;

import de.evaspringbuch.eva06smarthomeadvanced.advanced.domain.Building;
import jakarta.persistence.Entity;

@Entity
public class PrivateHouse extends Building {

	private int children;

	public PrivateHouse() {
	}

	public PrivateHouse withChildren(int children) {
		this.children = children;
		return this;
	}
}
