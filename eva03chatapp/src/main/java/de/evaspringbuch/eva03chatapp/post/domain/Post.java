package de.evaspringbuch.eva03chatapp.post.domain;

import de.evaspringbuch.eva03chatapp.chat.domain.Chat;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Post {

	@Id
	@GeneratedValue
	private Integer id;

	private String content;

	private String timestamp;

	private String type;

	public Post() {
		this.content = "";
		this.timestamp = "";
		this.type = "";
	}

	public Post(String content, Chat chat, String type) {
		this.content = content;
		this.type = type;
//        this.chat = chat;
	}

	public String getContent() {
		return content;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}
}
