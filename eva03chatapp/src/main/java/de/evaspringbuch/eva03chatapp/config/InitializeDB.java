package de.evaspringbuch.eva03chatapp.config;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.evaspringbuch.eva03chatapp.chat.domain.Chat;
import de.evaspringbuch.eva03chatapp.chat.domain.ChatRepository;
import de.evaspringbuch.eva03chatapp.chat.domain.ChatUser;
import de.evaspringbuch.eva03chatapp.chat.domain.ChatUserRepository;
import de.evaspringbuch.eva03chatapp.post.domain.Post;
import de.evaspringbuch.eva03chatapp.post.domain.PostRepository;
import jakarta.annotation.PostConstruct;

@Component
public class InitializeDB {

	private static final Logger log = LoggerFactory.getLogger(InitializeDB.class);

	@Autowired
	ChatUserRepository chatUserRepository;
	@Autowired
	ChatRepository chatRepository;
	@Autowired
	PostRepository postRepository;

	@PostConstruct
	public void init() {

		log.debug("Db initialized");

		ChatUser chatUser1 = createChatUser("elisa");
		ChatUser chatUser2 = createChatUser("marga");
		ChatUser chatUser3 = createChatUser("frieda");

		Chat chat1 = createChat(chatUser2.getNickname(), chatUser1);
		Chat chat2 = createChat(chatUser1.getNickname(), chatUser2);

		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)
				.withLocale(Locale.GERMAN);
		LocalTime loc = LocalTime.of(19, 44, 0, 0);

		String s = loc.minusMinutes(10).plusSeconds(4).format(germanFormatter);
		createPost("Hallo Marga", s, chat1, "out");
		createPost("Hallo Marga", s, chat2, "in");

		s = loc.minusMinutes(8).plusSeconds(42).format(germanFormatter);
		createPost("Hi Elisa", s, chat1, "in");
		createPost("Hi Elisa", s, chat2, "out");

		s = loc.minusMinutes(6).plusSeconds(33).format(germanFormatter);
		createPost("Hab heute spring gelernt", s, chat1, "in");
		createPost("Hab heute spring gelernt", s, chat2, "out");

		s = loc.minusMinutes(5).plusSeconds(5).format(germanFormatter);
		createPost("wie hoch bist du gekommen?", s, chat1, "out");
		createPost("wie hoch bist du gekommen?", s, chat2, "in");

		s = loc.minusMinutes(3).plusSeconds(17).format(germanFormatter);
		createPost("mit 5 bohnen ging es ganz gut", s, chat1, "in");
		createPost("mit 5 bohnen ging es ganz gut", s, chat2, "out");

		s = loc.plusMinutes(1).plusSeconds(38).format(germanFormatter);
		createPost("ich musste 12 nehmen puh", s, chat1, "out");
		createPost("ich musste 12 nehmen puh", s, chat2, "in");

		s = loc.plusMinutes(4).plusSeconds(2).format(germanFormatter);
		createPost("du musst auch immer Uebertreiben", s, chat1, "in");
		createPost("du musst auch immer Uebertreiben", s, chat2, "out");

		chatRepository.save(chat1);
		chatRepository.save(chat2);
	}

	private ChatUser createChatUser(String nickname) {
		ChatUser chatUser = new ChatUser();
		chatUser.setNickname(nickname);
		chatUserRepository.save(chatUser);
		return chatUser;
	}
	
	private Chat createChat(String nickname, ChatUser chatUser) {
		Chat chat = new Chat(nickname, chatUser);
		chatUser.addChat(chat);
		chatRepository.save(chat);
//		try {
//			chatRepository.save(chat);
//			log.info("Chat between {} and {} created successfully", nickname, chatUser.getNickname());
//		} catch (Exception e) {
//			log.error("Error creating Chat between {} and {}: {}", nickname, chatUser.getNickname(), e.getMessage());
//		}
		return chat;
	}
	

	private void createPost(String content, String timestamp, Chat chat, String inOrOut) {
		Post post = new Post(content, chat, inOrOut);
		post.setTimestamp(timestamp);
		postRepository.save(post);
		chat.addPosts(post);
//            if ("out".equals(inOrOut)) {
//                chat.addNewPosts();
//            }
	}
}