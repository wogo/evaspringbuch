package de.evaspringbuch.eva09aop.tryAopProxy;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class SomeEntity implements Serializable {

    @Id @GeneratedValue
    private int id;

    public SomeEntity() {
    }

}
