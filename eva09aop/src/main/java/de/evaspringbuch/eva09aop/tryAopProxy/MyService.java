package de.evaspringbuch.eva09aop.tryAopProxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
//@Qualifier("zwei")
public class MyService implements MyServiceBase {

    @Autowired
    private SomeEntityRepository someEntityRepository;

    @Transactional
    public void doSomething() {

        someEntityRepository.save(new SomeEntity());
        System.out.println("   > MyService < ");
    }
}
