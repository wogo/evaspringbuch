package de.evaspringbuch.eva09aop.tryAopProxy;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CglibService {

    @Transactional
    public void doSomething() {
        System.out.println("   > CglibService < ");
    }
}
