package de.evaspringbuch.eva07chatapp.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.evaspringbuch.eva07chatapp.chat.domain.Chat;
import de.evaspringbuch.eva07chatapp.chat.domain.ChatRepository;
import de.evaspringbuch.eva07chatapp.chat.domain.ChatUser;
import de.evaspringbuch.eva07chatapp.chat.domain.ChatUserRepository;
import de.evaspringbuch.eva07chatapp.post.domain.Post;
import de.evaspringbuch.eva07chatapp.post.domain.PostRepository;
import de.evaspringbuch.eva07chatapp.security.domain.Role;
import de.evaspringbuch.eva07chatapp.security.domain.User;
import de.evaspringbuch.eva07chatapp.security.domain.UserRepository;
import jakarta.annotation.PostConstruct;

@Component
public class InitializeDB {

	private static final Logger log = LoggerFactory.getLogger(InitializeDB.class);

	@Autowired
	ChatUserRepository chatUserRepository;
	@Autowired
	ChatRepository chatRepository;
	@Autowired
	PostRepository postRepository;
	@Autowired
	UserRepository userRepository;

	@PostConstruct
	public void init() {
		log.debug("Db initialized");

		createUser("elisa", "el@a", "demo", Role.ADMIN);
		createUser("marga", "ma@a", "demo", Role.USER);
		createUser("frieda", "fr@a", "demo", Role.USER);

		ChatUser chatUser1 = createChatUser("elisa");
		ChatUser chatUser2 = createChatUser("marga");
		ChatUser chatUser3 = createChatUser("frieda");

		Chat chat1 = createChat(chatUser2.getNickname(), chatUser1);
		Chat chat2 = createChat(chatUser1.getNickname(), chatUser2);

		createPost("Hallo Marga", chat1, "out");
		createPost("Hallo Marga", chat2, "in");

		createPost("Hi Elisa", chat1, "in");
		createPost("Hi Elisa", chat2, "out");
	}

	private void createUser(String nickname, String email, String password, Role role) {
		User user = new User();
		user.setNickname(nickname);
		user.setEmail(email);
		user.setPassword(password);
		user.setRole(role);
		userRepository.save(user);
	}

	private ChatUser createChatUser(String nickname) {
		ChatUser chatUser = new ChatUser();
		chatUser.setNickname(nickname);
		chatUserRepository.save(chatUser);
		return chatUser;
	}

	private Chat createChat(String nickname, ChatUser chatUser) {
		Chat chat = new Chat(nickname, chatUser);
		chatUser.addChat(chat);
		chatRepository.save(chat);
//		try {
//			chatRepository.save(chat);
//			log.info("Chat between {} and {} created successfully", nickname, chatUser.getNickname());
//		} catch (Exception e) {
//			log.error("Error creating Chat between {} and {}: {}", nickname, chatUser.getNickname(), e.getMessage());
//		}
		return chat;
	}

	private void createPost(String content, Chat chat, String inOrOut) {
		Post post = new Post(content, chat, inOrOut);
		postRepository.save(post);
		chat.addPosts(post);
		if ("out".equals(inOrOut)) {
			chat.addNewPosts();
		}
	}

}