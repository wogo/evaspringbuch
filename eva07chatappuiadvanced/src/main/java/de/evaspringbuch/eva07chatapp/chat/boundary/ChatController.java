package de.evaspringbuch.eva07chatapp.chat.boundary;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import de.evaspringbuch.eva07chatapp.chat.service.ChatUserService;
import de.evaspringbuch.eva07chatapp.chat.service.NewChatPossible;
import de.evaspringbuch.eva07chatapp.chat.service.dto.ChatDTO;
import de.evaspringbuch.eva07chatapp.security.domain.CurrentUser;
import de.evaspringbuch.eva07chatapp.security.domain.User;
import de.evaspringbuch.eva07chatapp.security.service.user.UserService;

@Controller
public class ChatController {

	private static final Logger log = LoggerFactory.getLogger(ChatController.class);

	private ChatUserService chatUserService;

	private UserService userService;

	private CurrentUser currentUser;

	@Autowired
	public ChatController(ChatUserService chatUserService, UserService userService, CurrentUser currentUser) {
		this.chatUserService = chatUserService;
		this.userService = userService;
		this.currentUser = currentUser;
	}

	@PostMapping("/first")
	public String firstPage(@RequestParam String email, @RequestParam String password, Model model) {
		Optional<User> user = userService.getUserByEmail(email);
		if (user.isEmpty() || !password.equals(user.get().getPassword())) {
			model.addAttribute("error", "falscher Login");
			return "login";
		}
		currentUser.setUser(user.get());
		model.addAttribute("fromUser", currentUser.getNickname());
		model.addAttribute("listAllChats", chatUserService.getAllChatFrom(currentUser.getNickname()));
		return "chat";
	}

	@RequestMapping(value = "/start", method = { RequestMethod.POST, RequestMethod.GET })
	public String startPage(@RequestParam String fromUser, Model model) {
		String from = getCurrentUser(model);
		List<ChatDTO> targetList = chatUserService.getAllChatFrom(from);
		model.addAttribute("listAllChats", targetList);
		return "chat";
	}

	@RequestMapping(value = "/newchat", method = { RequestMethod.POST, RequestMethod.GET })
	public String newChatPage(@RequestParam("nid") String to, Model model) {
		String from = getCurrentUser(model);
		NewChatPossible newChatPossible = chatUserService.aNewChatPossible(from, to);
		if (newChatPossible.possible()) {
			chatUserService.newChatFromTo(from, to);
		} else
			model.addAttribute("error", newChatPossible.message());
		List<ChatDTO> targetList = chatUserService.getAllChatFrom(from);
		model.addAttribute("listAllChats", targetList);
		return "newchat";

	}

	@PostMapping("/deleteChat")
	public String removeChat(@RequestParam String to, Model model) {
		String from = getCurrentUser(model);
		chatUserService.deleteChatFromTo(from, to);
		return "redirect:start?fromUser=" + from;
	}

	private String getCurrentUser(Model model) {
		String from = currentUser.getUser().getNickname();
		model.addAttribute("fromUser", from);
		return from;
	}

}
