package de.evaspringbuch.eva07chatapp.chat.service.dto;

public record ChatDTO(String chatWith, int newPosts) {
}
