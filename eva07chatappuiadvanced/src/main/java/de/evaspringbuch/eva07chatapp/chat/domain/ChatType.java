package de.evaspringbuch.eva07chatapp.chat.domain;

public enum ChatType {
	NORMAL, BOT
}
