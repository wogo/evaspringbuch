package de.evaspringbuch.eva08corsback.security.service.user;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.evaspringbuch.eva08corsback.security.domain.User;
import de.evaspringbuch.eva08corsback.security.domain.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	private final UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User getUserById(long id) {
		log.debug("Getting user={}", id);
		return userRepository.findById(id).get();
	}

	@Override
	public User getUserByEmail(String email) {
		log.debug("Getting user by email={}", email.replaceFirst("@.*", "@***"));
		return userRepository.findOneByEmail(email);
	}

	@Override
	public boolean existsByNickname(String nickname) {
		return userRepository.existsByNickname(nickname);
	}

	@Override
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public Collection<User> getAllUsers() {
		log.debug("Getting all users");
		return userRepository.findAllByOrderByEmailAsc();
	}

}
