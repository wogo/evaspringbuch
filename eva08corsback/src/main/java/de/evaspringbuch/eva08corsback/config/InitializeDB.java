package de.evaspringbuch.eva08corsback.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import de.evaspringbuch.eva08corsback.security.domain.Role;
import de.evaspringbuch.eva08corsback.security.domain.User;
import de.evaspringbuch.eva08corsback.security.domain.UserRepository;
import jakarta.annotation.PostConstruct;

@Component
public class InitializeDB {

    private static final Logger log = LoggerFactory.getLogger(InitializeDB.class);

    @Autowired
    UserRepository userRepository;

    @PostConstruct
    public void init() {
        log.debug("Db initialized");

        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        createUser("elisa", "el@a", "demo", Role.ADMIN, passwordEncoder);
        createUser("marga", "ma@a", "demo", Role.USER, passwordEncoder);
        createUser("frieda", "fr@a", "demo", Role.USER, passwordEncoder);
    }

    private void createUser(String nickname, String email, String password, Role role, PasswordEncoder passwordEncoder) {
        User user = new User();
        user.setNickname(nickname);
        user.setEmail(email);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRole(role);
        try {
            userRepository.save(user);
            log.info("User {} created successfully", nickname);
        } catch (Exception e) {
            log.error("Error creating user {}: {}", nickname, e.getMessage());
        }
    }
}
