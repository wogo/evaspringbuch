package de.evaspringbuch.eva07chatapp.chat.service;

public record NewChatPossible(Boolean possible, String message) {

}
