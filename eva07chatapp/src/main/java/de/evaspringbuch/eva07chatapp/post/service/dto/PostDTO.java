package de.evaspringbuch.eva07chatapp.post.service.dto;

public record PostDTO(String content, String timestamp, String type) {
}
