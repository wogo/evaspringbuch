package de.evaspringbuch.eva07chatapp.security.service.dto;

public record UserDTO(
		Long id, String nickname, String email) {
}