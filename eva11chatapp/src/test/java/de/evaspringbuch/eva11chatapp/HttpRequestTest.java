package de.evaspringbuch.eva11chatapp;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.evaspringbuch.eva11chatapp.security.domain.CurrentUser;
import de.evaspringbuch.eva11chatapp.security.domain.Role;
import de.evaspringbuch.eva11chatapp.security.domain.User;
import de.evaspringbuch.eva11chatapp.security.domain.UserCreateForm;

@SpringBootTest
@AutoConfigureMockMvc
class HttpRequestTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	private User user;

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
		user = new User();
		user.setNickname("elisa");
		user.setEmail("el@a");
		user.setPasswordHash(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("demo"));
		user.setRole(Role.ADMIN);
	}

	@Test
	public void testHandleUserCreateForm() throws Exception {

		UserCreateForm form = new UserCreateForm();
		form.setNickname("newuser");
		form.setEmail("newuser@example.com");
		form.setPassword("password");
		form.setPasswordRepeated("password");
		form.setRole(Role.USER);

		mockMvc.perform(post("/users/create").with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(SecurityMockMvcRequestPostProcessors.user(new CurrentUser(user))).flashAttr("myform", form))
				.andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/users/managed"));
	}

	@Test
	public void testHandleUserCreateFormWithErrors() throws Exception {
		UserCreateForm form = new UserCreateForm();
		form.setNickname("newuser");
		form.setEmail("newuser@example.com");
		form.setPassword("password");
		form.setPasswordRepeated("mismatch"); // Intentional error
		form.setRole(Role.USER);

		mockMvc.perform(post("/users/create").with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(SecurityMockMvcRequestPostProcessors.user(new CurrentUser(user))).flashAttr("myform", form))
				.andExpect(status().isOk()).andExpect(view().name("user_create"))
				.andExpect(model().attributeExists("error"));
	}
}
