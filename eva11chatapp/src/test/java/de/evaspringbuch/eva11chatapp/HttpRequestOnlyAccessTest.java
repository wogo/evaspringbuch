package de.evaspringbuch.eva11chatapp;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.evaspringbuch.eva11chatapp.security.domain.CurrentUser;
import de.evaspringbuch.eva11chatapp.security.domain.Role;
import de.evaspringbuch.eva11chatapp.security.domain.User;

@SpringBootTest
class HttpRequestOnlyAccessTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
	}

	@Test
	public void testAccess() throws Exception {

		User user = new User();
		user.setNickname("elisa");
		user.setEmail("el@a");
		user.setPasswordHash(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("demo"));
		user.setRole(Role.ADMIN);

		mockMvc.perform(
//				 	post("/add")
				get("/users/managed").with(SecurityMockMvcRequestPostProcessors.user(new CurrentUser(user))))
				.andExpect(status().is2xxSuccessful()).andExpect(view().name("user_create"));
	}

}
