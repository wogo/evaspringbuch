package de.evaspringbuch.eva11chatapp.chat.service;

public record NewChatPossible(Boolean possible, String message) {

}
