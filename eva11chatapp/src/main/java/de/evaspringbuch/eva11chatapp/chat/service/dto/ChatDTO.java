package de.evaspringbuch.eva11chatapp.chat.service.dto;

public record ChatDTO(String chatWith, int newPosts) {
}
