package de.evaspringbuch.eva11chatapp.chat.boundary;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import de.evaspringbuch.eva11chatapp.chat.service.ChatUserService;
import de.evaspringbuch.eva11chatapp.chat.service.NewChatPossible;
import de.evaspringbuch.eva11chatapp.chat.service.dto.ChatDTO;
import de.evaspringbuch.eva11chatapp.common.CurrentUserUtil;

@Controller
public class ChatController {

	private static final Logger log = LoggerFactory.getLogger(ChatController.class);

	private ChatUserService chatUserService;

	@Autowired
	public ChatController(ChatUserService chatUserService) {
		this.chatUserService = chatUserService;
	}

	@RequestMapping(value = "/first", method = { RequestMethod.GET, RequestMethod.POST })
	public String firstPage(Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		}

		String from = CurrentUserUtil.getCurrentUser(model);
		List<ChatDTO> targetList = chatUserService.getAllChatFrom(from);
		model.addAttribute("fromUser", from);
		model.addAttribute("listAllChats", targetList);
		return "chat";
	}

	@RequestMapping(value = "/newchat", method = { RequestMethod.GET, RequestMethod.POST })
	public String newChatPage(@RequestParam("nid") String to, Model model) {
		String from = CurrentUserUtil.getCurrentUser(model);
		NewChatPossible newChatPossible = chatUserService.aNewChatPossible(from, to);
		if (newChatPossible.possible()) {
			chatUserService.newChatFromTo(from, to);
		} else
			model.addAttribute("error", newChatPossible.message());
		List<ChatDTO> targetList = chatUserService.getAllChatFrom(from);
		model.addAttribute("listAllChats", targetList);
		return "newchat";
	}

	@PostMapping("/deleteChat")
	public String removeChat(@RequestParam String to, Model model) {
		String from = CurrentUserUtil.getCurrentUser(model);
		chatUserService.deleteChatFromTo(from, to);
		return "redirect:first";
	}

}
