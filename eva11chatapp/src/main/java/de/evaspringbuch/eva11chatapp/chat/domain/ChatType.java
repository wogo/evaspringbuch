package de.evaspringbuch.eva11chatapp.chat.domain;

public enum ChatType {
	NORMAL, BOT
}
