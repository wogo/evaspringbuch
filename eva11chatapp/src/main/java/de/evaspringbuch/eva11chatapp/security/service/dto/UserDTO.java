package de.evaspringbuch.eva11chatapp.security.service.dto;

public record UserDTO(

		Long id, String nickname, String email) {
}
