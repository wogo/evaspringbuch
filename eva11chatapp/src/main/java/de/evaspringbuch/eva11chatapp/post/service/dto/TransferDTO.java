package de.evaspringbuch.eva11chatapp.post.service.dto;

import java.io.Serializable;

public record TransferDTO(String to, int amount) implements Serializable {}
