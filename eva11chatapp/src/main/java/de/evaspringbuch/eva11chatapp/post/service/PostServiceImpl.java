package de.evaspringbuch.eva11chatapp.post.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.evaspringbuch.eva11chatapp.chat.domain.Chat;
import de.evaspringbuch.eva11chatapp.chat.domain.ChatUser;
import de.evaspringbuch.eva11chatapp.chat.service.ChatService;
import de.evaspringbuch.eva11chatapp.chat.service.ChatUserService;
import de.evaspringbuch.eva11chatapp.post.domain.Post;
import de.evaspringbuch.eva11chatapp.post.service.dto.PayActionResponseDTO;
import de.evaspringbuch.eva11chatapp.post.service.dto.PostDTO;

@Service
public class PostServiceImpl implements PostService {

//	private static final Logger log = LoggerFactory.getLogger(PostServiceImpl.class);

	private ChatUserService chatUserService;
	private ChatService chatService;
	private SmmpService smmpService;

	@Autowired
	public PostServiceImpl(ChatUserService chatUserService, ChatService chatService, SmmpService smmpService) {
		this.chatUserService = chatUserService;
		this.chatService = chatService;
		this.smmpService = smmpService;
	}

	@Override
	public List<PostDTO> listAllPostsFromTo(String from, String to) {
		ChatUser chatuserFrom = chatUserService.getByNickname(from);
		chatService.resetNewPosts(from, to);
		Map<String, Chat> fromChats = chatuserFrom.getChats();
		List<Post> posts = fromChats.get(to).getPosts();
		return posts.stream().map(source -> new PostDTO(source.getContent(), source.getTimestamp(), source.getType()))
				.collect(Collectors.toList());
	}

	@Override
	public void addPost(String from, String to, String pcontent) {
		Chat chatFrom = chatUserService.getChatFromByNicknameTo(to, from);
		chatService.savePosts(chatFrom, pcontent, "out");
		if ("smmp".equals(to)) {
			PayActionResponseDTO payActionResponse = smmpService.doPayAction(from, to, pcontent);
			chatService.savePosts(chatFrom, payActionResponse.getDescription(), "in");
		} else {
			Chat chatTo = chatUserService.getChatFromByNicknameTo(from, to);
			if (chatTo != null) {
				chatTo.addNewPosts();
				chatService.savePosts(chatTo, pcontent, "in");
			} else {
				chatService.savePosts(chatFrom, "chat mit " + to + " ist gelöscht", "in");
			}
		}
	}

}
