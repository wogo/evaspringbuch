package de.evaspringbuch.eva11chatapp.post.service.dto;

public record PostDTO(String content, String timestamp, String type) {}
