package de.evaspringbuch.eva11chatapp.post.service.dto;

public record AccountResponseDTO(String code) {}