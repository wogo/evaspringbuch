package de.evaspringbuch.eva11chatapp.config;

import static org.springframework.security.config.Customizer.withDefaults;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity // (debug = true)
@EnableMethodSecurity(prePostEnabled = true)
class WebSecurityConfig {

//	@Autowired
//	private UserDetailsService userDetailsService;

	@Bean
	WebSecurityCustomizer webSecurityCustomizer() {
		return (web) -> web.ignoring().requestMatchers("/h2-console/**").requestMatchers("/console/**");
	}

	@Bean
	protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//		http.csrf(csrf -> csrf.disable());
		http.authorizeHttpRequests(
				authorize -> authorize.requestMatchers("/login", "/logout*", "/css/**", "/username", "/debug/auth")
						.permitAll().requestMatchers("/users/m**").hasAuthority("ADMIN").requestMatchers("/users/c**")
						.hasAuthority("ADMIN").anyRequest().authenticated())
				.formLogin(formLogin -> formLogin.loginPage("/login").defaultSuccessUrl("/first", true)
						.failureUrl("/login?error").usernameParameter("email").permitAll())
				.logout(logout -> logout.logoutSuccessUrl("/logoutbye").invalidateHttpSession(true)
						.deleteCookies("JSESSIONID"))
				.rememberMe(withDefaults());

		return http.build();
	}

//	@Bean
//	public AuthenticationManager authenticationManager(UserDetailsService userDetailsService,
//			PasswordEncoder passwordEncoder) {
//		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
//		authenticationProvider.setUserDetailsService(userDetailsService);
//		authenticationProvider.setPasswordEncoder(passwordEncoder);
//		return new ProviderManager(authenticationProvider);
//	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}