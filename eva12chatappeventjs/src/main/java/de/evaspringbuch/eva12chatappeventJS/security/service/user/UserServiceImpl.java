package de.evaspringbuch.eva12chatappeventJS.security.service.user;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import de.evaspringbuch.eva12chatappeventJS.security.domain.User;
import de.evaspringbuch.eva12chatappeventJS.security.domain.UserCreateForm;
import de.evaspringbuch.eva12chatappeventJS.security.domain.UserRepository;
import de.evaspringbuch.eva12chatappeventJS.security.service.dto.UserDTO;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	private UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDTO getUserById(long id) {
		log.debug("Getting user={}", id);
		User user = userRepository.findById(id)
				.orElseThrow(() -> new NoSuchElementException(String.format(">>> User=%s not found", id)));
		UserDTO userDTO = new UserDTO(user.getId(), user.getNickname(), user.getEmail());
		return userDTO;
	}

	@Override
	public Optional<User> getUserByEmail(String email) {
		log.debug("Getting user by email={}", email.replaceFirst("@.*", "@***"));
		return userRepository.findOneByEmail(email);
	}

	@Override
	public boolean existsByNickname(String nickname) {
		return userRepository.existsByNickname(nickname);
	}

	@Override
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public List<UserDTO> getAllUsers() {
		log.debug("Getting all users");
		return userRepository.findAllByOrderByNicknameAsc().stream()
				.map(source -> new UserDTO(source.getId(), source.getNickname(), source.getEmail()))
				.collect(Collectors.toList());
	}

	@Override
	public User create(UserCreateForm form) {
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		User user = new User();
		user.setEmail(form.getEmail());
		user.setNickname(form.getNickname());
		user.setPasswordHash(passwordEncoder.encode(form.getPassword()));
		user.setRole(form.getRole());
		return userRepository.save(user);
	}

}
