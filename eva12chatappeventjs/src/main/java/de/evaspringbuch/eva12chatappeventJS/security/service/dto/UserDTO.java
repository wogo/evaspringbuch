package de.evaspringbuch.eva12chatappeventJS.security.service.dto;

public record UserDTO(

		Long id, String nickname, String email) {
}
