package de.evaspringbuch.eva12chatappeventJS.post.service;

public record PostReadEvent(String from, String to) {
}