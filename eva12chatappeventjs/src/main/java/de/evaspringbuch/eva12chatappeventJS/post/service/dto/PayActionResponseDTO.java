package de.evaspringbuch.eva12chatappeventJS.post.service.dto;

public class PayActionResponseDTO {
	private boolean payment;
	private String description = "";

	public PayActionResponseDTO() {
	}

	public PayActionResponseDTO payment(boolean payment) {
		this.payment = payment;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public PayActionResponseDTO description(String description) {
		this.description = description;
		return this;
	}

}
