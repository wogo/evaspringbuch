package de.evaspringbuch.eva12chatappeventJS.post.service.dto;

public record AccountResponseDTO(String code) {
}
