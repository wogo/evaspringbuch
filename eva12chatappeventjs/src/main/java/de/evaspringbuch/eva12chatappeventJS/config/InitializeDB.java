package de.evaspringbuch.eva12chatappeventJS.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import de.evaspringbuch.eva12chatappeventJS.chat.domain.Chat;
import de.evaspringbuch.eva12chatappeventJS.chat.domain.ChatRepository;
import de.evaspringbuch.eva12chatappeventJS.chat.domain.ChatType;
import de.evaspringbuch.eva12chatappeventJS.chat.domain.ChatUser;
import de.evaspringbuch.eva12chatappeventJS.chat.domain.ChatUserRepository;
import de.evaspringbuch.eva12chatappeventJS.post.domain.Post;
import de.evaspringbuch.eva12chatappeventJS.post.domain.PostRepository;
import de.evaspringbuch.eva12chatappeventJS.security.domain.Role;
import de.evaspringbuch.eva12chatappeventJS.security.domain.User;
import de.evaspringbuch.eva12chatappeventJS.security.domain.UserRepository;
import jakarta.annotation.PostConstruct;

@Component
public class InitializeDB {

	private static final Logger log = LoggerFactory.getLogger(InitializeDB.class);

	@Autowired
	ChatUserRepository chatUserRepository;
	@Autowired
	ChatRepository chatRepository;
	@Autowired
	PostRepository postRepository;
	@Autowired
	UserRepository userRepository;

	@PostConstruct
	public void init() {
		log.debug("Db initialized");

		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

		createUser("elisa", "el@a", "demo", Role.ADMIN, passwordEncoder);
		createUser("marga", "ma@a", "demo", Role.USER, passwordEncoder);
		createUser("frieda", "fr@a", "demo", Role.USER, passwordEncoder);

		ChatUser chatUser1 = createChatUser("elisa", ChatType.NORMAL);
		ChatUser chatUser2 = createChatUser("marga", ChatType.NORMAL);
		ChatUser chatUser3 = createChatUser("frieda", ChatType.NORMAL);
		ChatUser chatBot = createChatUser("smmp", ChatType.BOT);

		Chat chat1 = createChat(chatUser2.getNickname(), chatUser1);
		Chat chat2 = createChat(chatUser1.getNickname(), chatUser2);
		Chat chat3 = createChat(chatBot.getNickname(), chatUser1);
		// Chat chat4 = createChat(chatUser1.getNickname(), chatBot);

		createPost("Hallo Marga", chat1, "out");
		createPost("Hallo Marga", chat2, "in");

		createPost("Hi Elisa", chat1, "in");
		createPost("Hi Elisa", chat2, "out");
	}

	private void createUser(String nickname, String email, String password, Role role,
			PasswordEncoder passwordEncoder) {
		User user = new User();
		user.setNickname(nickname);
		user.setEmail(email);
		user.setPasswordHash(passwordEncoder.encode(password));
		user.setRole(role);
		try {
			userRepository.save(user);
			log.info("User {} created successfully", nickname);
		} catch (Exception e) {
			log.error("Error creating user {}: {}", nickname, e.getMessage());
		}
	}

	private ChatUser createChatUser(String nickname, ChatType chatType) {
		ChatUser chatUser = new ChatUser();
		chatUser.setNickname(nickname);
		chatUser.setChatType(chatType);
		chatUserRepository.save(chatUser);
		return chatUser;
	}

	private Chat createChat(String nickname, ChatUser chatUser) {
		Chat chat = new Chat(nickname, chatUser);
		chatUser.addChat(chat);
		chatRepository.save(chat);
//		try {
//			chatRepository.save(chat);
//			log.info("Chat between {} and {} created successfully", nickname, chatUser.getNickname());
//		} catch (Exception e) {
//			log.error("Error creating Chat between {} and {}: {}", nickname, chatUser.getNickname(), e.getMessage());
//		}

		return chat;
	}

	private void createPost(String content, Chat chat, String inOrOut) {
		Post post = new Post(content, chat, inOrOut);
		postRepository.save(post);
		chat.addPosts(post);
//            if ("out".equals(inOrOut)) {
//                chat.addNewPosts();
//            }
	}
}
