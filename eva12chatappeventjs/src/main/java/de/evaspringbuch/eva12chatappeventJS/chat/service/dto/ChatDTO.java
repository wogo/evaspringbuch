package de.evaspringbuch.eva12chatappeventJS.chat.service.dto;

public record ChatDTO(String chatWith, int newPosts) {
}
