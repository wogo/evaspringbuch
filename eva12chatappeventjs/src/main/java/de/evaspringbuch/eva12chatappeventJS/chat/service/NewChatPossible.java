package de.evaspringbuch.eva12chatappeventJS.chat.service;

public record NewChatPossible(Boolean possible, String message) {

}