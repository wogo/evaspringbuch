package de.evaspringbuch.eva11mobpay.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.evaspringbuch.eva11mobpay.usermobpay.domain.PayUser;
import de.evaspringbuch.eva11mobpay.usermobpay.domain.PayUserRepository;
import de.evaspringbuch.eva11mobpay.usermobpay.domain.State;
import jakarta.annotation.PostConstruct;

@Component
public class InitializeDB {

    private static final Logger log = LoggerFactory.getLogger(InitializeDB.class);

    @Autowired private PayUserRepository payUserRepository;

    @PostConstruct
    public void init()  {

            log.debug("Db initialized");

            PayUser payUser = new PayUser();
            payUser.setName("elisa");
            payUser.setState(State.AVAILABLE);
            payUserRepository.save(payUser);

            payUser = new PayUser();
            payUser.setName("marga");
            payUser.setState(State.AVAILABLE); //doesNotExist);
            payUserRepository.save(payUser);

            payUser = new PayUser();
            payUser.setName("frieda");
            payUser.setState(State.SUSPENDED); //requested
            payUserRepository.save(payUser);

            payUser = new PayUser();
            payUser.setName("agneta");
            payUser.setState(State.SUSPENDED);
            payUserRepository.save(payUser);

    }
}
