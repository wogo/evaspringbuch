package de.evaspringbuch.eva08csrffront.security.service.dto;

public record UserDTO(Long id, String nickname, String email) {
}
