package de.evaspringbuch.eva08csrffront.chat.service.dto;

public record ChatDTO (String chatWith, int newPosts) {		
}

