package de.evaspringbuch.eva08csrffront.chat.domain;

public enum ChatType {
    NORMAL, BOT
}
