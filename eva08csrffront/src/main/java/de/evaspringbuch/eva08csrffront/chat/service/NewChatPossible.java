package de.evaspringbuch.eva08csrffront.chat.service;

public record NewChatPossible(Boolean possible, String message) {

}
