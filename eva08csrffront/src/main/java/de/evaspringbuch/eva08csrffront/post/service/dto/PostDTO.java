package de.evaspringbuch.eva08csrffront.post.service.dto;

public record PostDTO(String content,
	  	  String timestamp,
	        String type) { }