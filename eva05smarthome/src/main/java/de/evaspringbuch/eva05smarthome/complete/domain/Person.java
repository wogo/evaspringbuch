package de.evaspringbuch.eva05smarthome.complete.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;

@Entity
public class Person implements Serializable {

	@Id
	@GeneratedValue
	private Integer id;

	private String name;

	@ManyToMany
	private List<Building> ownedBuildings;

	public Person() {
		this.ownedBuildings = new ArrayList<>();
	}

	public Person addOwnedBuildings(Building building) {
		this.ownedBuildings.add(building);
		return this;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void removeBuilding(Building building) {
		this.ownedBuildings.remove(building);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		return this.getId() != null && this.getId().equals(other.getId());
	}
}
