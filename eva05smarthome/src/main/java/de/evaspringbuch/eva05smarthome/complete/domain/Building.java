package de.evaspringbuch.eva05smarthome.complete.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class Building implements Serializable {

	@Id
	@GeneratedValue
	private Integer id;

	@OneToMany
	private List<Room> rooms;

	@OneToOne
	private Address address;

	@ManyToMany(mappedBy = "ownedBuildings")
	private List<Person> owners;

	public Building() {
		this.rooms = new ArrayList<>();
		this.owners = new ArrayList<>();
	}

	public Integer getId() {
		return id;
	}

	public Building addRoom(Room room) {
		this.rooms.add(room);// .add(room);
		return this;
	}

	public Building withAddress(Address address) {
		this.address = address;
		return this;
	}

	public List<Person> getOwners() {
		return owners;
	}

	public Building addOwner(Person owner) {
		this.owners.add(owner);
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Building other = (Building) obj;
		return this.getId() != null && this.getId().equals(other.getId());
	}
}
