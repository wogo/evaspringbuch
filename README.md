# Repo zum Buch "Entwicklung verteilter Anwendungen, 2.A."
In der zweiten Auflage des Buches wird in die Entwicklung verteilter Anwendungen unter Verwendung des Spring Boot 3 Frameworks eingeführt. 
Das Buch ist im Springer-Verlag erscheinen, hier der Link zur [Buchseite vom Springer-Verlag](https://link.springer.com/book/9783658449933) 
und der Link zu [meiner Buchseite](https://evaspringbuch.de).

Die Code-Beispiele zu der ersten Auflage sind im Branch `springboot-2.6.8` und kleiner zu finden.

# Beispiel-Anwendung EvaChatApp
Als Demo-Anwendung wird eine sogenannte EvaChatApp sukzessive entwickelt. In jedem Kapitel werden neue Funktionalitäten hinzugefügt.

Der zur Verfügung gestellte Programmcode muss nicht immer sofort ausführbar sein, da teilweise nur Code-Fragmente eingestellt werden, die im Buch ausführlich erläutert werden. 

# Inhalt
Grundlagen

* Erste Schritte zur Anwendung

* Eine kleine Chat-Anwendung mit Spring MVC

* Ein Blick hinter die Kulissen von Dependency Injection

* Speichern von Model-Daten mit Spring Data

* Bearbeiten von Model-Daten mit Spring Data

* Die EvaChatApp unter der Haube

* Sicherheit geht vor – Schutz vor Angriffen und nicht autorisierten Zugriffen

* Aspektorientierte Programmierung (AOP)

* Transaktionen – alles oder nichts

* Rest – zwei Apps arbeiten Hand in Hand

* Events und Aktualisierung

# Eingesetzte Frameworks
Die Buch-Projekte stehen in folgenden Versionen zur Verfügung, wobei aktuell der main mit Spring Boot 3.2.3 gebuildet ist.

|  | **main** | **springboot-3.2.3** |  
| --- | :---: | :---: | 
| Spring Boot | **3.2.3** | 3.2.3 |
| Spring Framework | 6.1.4 | 6.1.4 |
| Spring Data | 2023.1.3 | 2023.1.3 | 
| Spring Security | 6.2.2 | 6.2.2 |
| Java | 21 oder 17 | 21 oder 17 |   
| JUnit | 5.10.2 | 5.10.2 | 
| Bootstrap | 5.3.2 | 5.3.2 |    
| gradle | 8.8 | 8.8 |   


**Anmerkungen**

* in allen Projekten, die jpa verwenden, ist in den application.properties die "alte" Datenbank-Konfiguration mit 
  `spring.datasource.generate-unique-name=false`   
  vorgenommen worden, da sich die Verwendung der h2-Console ohne weitere Konfiguration etwas geändert hat. Die JDBC-URL wird anders als früher festgelegt. Als JDBC-URL ist nun auf der Spring-Console folgender String  `jdbc:h2:mem:896ab7dd-c95f-4205-a0e7-b052f282b5b7` zu finden, wobei der letzte Teil (hinter `mem:`) eine UUID als Datenbankname darstellt. Den vollständigen String muss man als JDBC-URL verwenden. Bei jedem Neustart der Anwendung wird die JDBC-URL bzw. der Datenbankname neu generiert. Wer das nicht möchte, kann den ursprünglichen Default (Datenbankname `testdb`) mit einer der beiden Einstellungen in der `application.properties` festlegen
   * `spring.datasource.url=jdbc:h2:mem:testdb`
   * `spring.datasource.generate-unique-name=false`
   
* weiterhin wird in der application.properties durch 
    * `spring.jpa.properties.hibernate.column_ordering_strategy=legacy`

  festgelegt, dass Hibernate dieselbe Spaltenreihenfolge wie in früheren Versionen verwendet.
* in den Testklassen `@DirtiesContext(classMode=ClassMode.AFTER_CLASS)` bei den Annotationen der Klasse hinzugefügt. GRUND: wenn mehrere Testklassen ausgeführt werden sollen, wird jeweils der Kontext nach Durchführung aller Testmethoden einer Klasse zurückgesetzt. Damit werden Abhängigkeiten in der Ausführungsreihenfolge der Testklassen verhindert.

* Falls der Fehler `java.lang.IllegalArgumentException: Name for argument of type [java.util.Optional] not specified, and parameter name information not available via reflection. Ensure that the compiler uses the '-parameters' flag.` auftritt, versucht das Spring Framework, die Namen von Methodenparametern zu ermitteln, diese Informationen sind jedoch nicht verfügbar.

  Spring Boot 3 und Spring Framework 6 haben im Unterschied zu Spring Boot 2 strengere Anforderungen an die Verfügbarkeit von Methodenparametern.

  Mit Gradle (getestet mit Version 8.8) tritt dieser Fehler in der Regel nicht auf, da Gradle standardmäßig das `-parameters` Compiler-Flag setzt, das die erforderlichen Parameterinformationen speichert.

  Der Fehler kann jedoch in Entwicklungsumgebungen wie Spring Tool Suite und Eclipse auftreten. Um das Problem zu beheben, muss in den Compiler-Einstellungen die Option "Store information about method parameters (usable via reflection)" aktiviert werden, damit die Parameterinformationen korrekt gespeichert werden und der Fehler nicht mehr auftritt.

**Code downloaden oder mit git clonen**

für Main

    git clone https://gitlab.com/wogo/evaspringbuch.git

für Branch (z. B. für den Branch springboot-3.2.3)

    git clone -branch springboot-3.2.3 https://gitlab.com/wogo/evaspringbuch.git

Als Entwicklungsumgebungen können u.a. SpringToolsSuite 4 (damit sind die Projekte entwickelt worden), Eclipse, IntelliJ, Visual Studio Code sowie ... verwendet werden.

Als Build-Managment-Tool kommt gradle zum Einsatz.
